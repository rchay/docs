# SPC Troisième

## Fiches d'exercices

- Chapitre 01 - Les changements d'états [[sujet]](pdf/exo/troisieme_exercices_01_changements_etats.pdf){target=_blank}
- Chapitre 02 - La masse volumique [[sujet]](pdf/exo/troisieme_exercices_02_exercices_masse_volumique.pdf){target=_blank}
- Chapitre 03 - Le système solaire et la structure de l'Univers [[sujet]](pdf/exo/troisieme_exercices_03_vitesse_de_la_lumiere.pdf){target=_blank}
- Chapitre 04 - Description du mouvement
- Chapitre 05 - Interactions et forces [[sujet]](pdf/exo/troisieme_exercices_05_forces.pdf){target=_blank}
- Chapitre 06 - Poids et masse [[sujet]](pdf/exo/troisieme_exercices_06_poids.pdf){target=_blank}
- Chapitre 07 - Le modèle de l'atome [[sujet]](pdf/exo/troisieme_exercices_07_atome.pdf){target=_blank}
- Chapitre 08 - Les ions [[sujet]](pdf/exo/troisieme_exercices_08_ions.pdf){target=_blank}
- Chapitre 09 - Solutions acides et solutions basiques [[sujet]](pdf/exo/troisieme_exercices_09_acides_bases.pdf){target=_blank}
- Chapitre 10 - Exemples de réactions chimiques [[sujet]](pdf/exo/troisieme_exercices_10_exercices_reaction_chimique.pdf){target=_blank}
- Chapitre 11 - Énergie et mouvements
- Chapitre 12 - La loi d'Ohm [[sujet]](pdf/exo/troisieme_exercices_12_loi_d_ohm.pdf){target=_blank}
- Chapitre 13 - Puissance et énergie électrique [[sujet]](pdf/exo/troisieme_exercices_13_puissance_electrique.pdf){target=_blank}

## Ressources

- Chapitre 01 - Les changements d'états
- Chapitre 02 - La masse volumique
- Chapitre 03 - Le système solaire et la structure de l'Univers
    - Si la Lune ne faisait qu'un pixel [[site]](https://joshworth.com/dev/pixelspace/pixelspace_solarsystem.html){target=_blank}
    - Scale of the Universe [[site]](https://scaleofuniverse.com/fr){target=_blank}
- Chapitre 04 - Description du mouvement
- Chapitre 05 - Interactions et forces
- Chapitre 06 - Poids et masse
- Chapitre 07 - Le modèle de l'atome
    - Modèles de l'atome [[animation]](swf/modeles_atomes.swf)
    - Expérience de Rutherford [[animation]](swf/rutherford.swf)
- Chapitre 08 - Les ions
- Chapitre 09 - Solutions acides et solutions basiques
    - Échelle de pH - [[animation]](https://phet.colorado.edu/sims/html/ph-scale/latest/ph-scale_all.html?locale=fr){target=_blank}
- Chapitre 10 - Exemples de réactions chimiques
    - Équilibrer des réactions chimiques [[animation]](https://phet.colorado.edu/sims/html/balancing-chemical-equations/latest/balancing-chemical-equations_fr.html){target=_blank}
- Chapitre 11 - Énergie et mouvements
    - Pas de ceinture de sécurite à 50 km/h [[vidéo]](vid/pas_de_ceinture_de_securite_a_50_kmh.mp4){target=_blank}
    - Énergie cinétique et vitesse [[vidéo]](https://www.youtube.com/watch?v=zlyIYtWWCkg){target=_blank}
    - Énergie potentielle de position [[vidéo]](https://www.youtube.com/watch?v=jrgvgeOgmt8){target=_blank}
    - Énergie au skate park [[animation]](https://phet.colorado.edu/sims/html/energy-skate-park-basics/latest/energy-skate-park-basics_all.html?locale=fr){target=_blank}
- Chapitre 12 - La loi d'Ohm
- Chapitre 13 - Puissance et énergie électrique

## Devoirs

### Devoirs 2023-2024

| Chapitres | Thèmes | Fichiers |
|:---------:|-------|--------|
|     1 et 2     |   Changements d'états, masse volumique    |   [[sujet]](pdf/2023/2023_10_11_devoir_masse_volumique_changements_etats.pdf){target=_blank} [[correction]](pdf/2023/2023_10_11_devoir_masse_volumique_changements_etats_correction.pdf){target=_blank}
|  2 et 3   |   Brevet blanc n°1 |   [[correction]](pdf/2023/2024_01_23_devoir_brevet_blanc_1_correction.pdf){target=_blank}
|     6    |   Poids et masse, gravitation universelle |   [[sujet]](pdf/2023/2024_02_06_devoir_poids_masse_gravitation.pdf){target=_blank} [[correction]](pdf/2023/2024_02_06_devoir_poids_masse_gravitation_correction.pdf){target=_blank}
|     8    |   Ions |   [[sujet]](pdf/2023/2024_04_08_devoir_ions.pdf){target=_blank} [[correction]](pdf/2023/2024_04_08_devoir_ions_correction.pdf){target=_blank}
|     8    |   Ions |   [[sujet]](pdf/2023/2024_04_09_devoir_ions.pdf){target=_blank} [[correction]](pdf/2023/2024_04_09_devoir_ions_correction.pdf){target=_blank}

### Devoirs 2022-2023

| Chapitres | Thèmes | Fichiers |
|:---------:|-------|--------|
|     1 et 2     |   Grandeurs physiques, changements d'états, masse volumique    |   [[sujet]](pdf/2022/2022_10_07_devoir_masse_volumique_changements_etats.pdf){target=_blank} [[correction]](pdf/2022/2022_10_07_devoir_masse_volumique_changements_etats_correction.pdf){target=_blank}
|     4     |   Calculs de vitesses, distances et temps    |   [[sujet]](pdf/2022/2022_11_17_devoir_vitesse_lumiere.pdf){target=_blank} [[correction]](pdf/2022/2022_11_18_devoir_distances_vitesse_lumiere_correction.pdf){target=_blank}
|     4, 5 et 6     |   Mouvements, forces, poids    |   [[sujet]](pdf/2022/2023_02_10_devoir_forces_mouvement_poids.pdf){target=_blank} [[correction]](pdf/2022/2023_02_10_devoir_forces_mouvement_poids_correction.pdf){target=_blank}
| 7 et 8 |   Atomes, ions, tableau périodique    |   [[sujet]](pdf/2022/2023_04_01_atome_ion_tableau_periodique.pdf){target=_blank} [[correction]](pdf/2022/2023_04_01_atome_ion_tableau_periodique_correction.pdf){target=_blank}
| 9 et 11  | Solutions acides et basiques, pH, énergie cinétique et énergie potentielle |   [[sujet]](pdf/2022/2023_06_02_acides_bases_energie_mouvement.pdf){target=_blank} [[correction]](pdf/2022/2023_06_02_acides_bases_energie_mouvement_correction.pdf){target=_blank}

### Devoirs 2021-2022

| Chapitres | Thèmes | Fichiers |
|:---------:|-------|--------|
| 1 et 2     |   Grandeurs physiques, changements d'états, masse volumique     |   [[sujet]](pdf/2021/2021_10_11_devoir_masse_volumique_changements_etats.pdf){target=_blank} [[correction]](pdf/2021/2021_10_11_devoir_masse_volumique_changements_etats_correction.pdf){target=_blank}|
|   4     |   Calculs de vitesses, distances et temps, année-lumière       |   [[sujet]](pdf/2021/2021_11_11_devoir_distances_vitesse_lumiere_.pdf){target=_blank} [[correction]](pdf/2021/2021_11_11_devoir_distances_vitesse_lumiere_correction.pdf){target=_blank}|
|  3, 4 , 7 et 10 | Système solaire, calculs de distance, vitesses et temps, molécules, tableau périodique,<br> transformations chimiques  (Brevet blanc 1)   |  [[sujet]](pdf/2021/2022_02_02_brevet_blanc.pdf){target=_blank}  [[correction]](pdf/2021/2022_02_02_brevet_blanc_correction.pdf){target=_blank}|
| 4, 5 et 6     |   Mouvements, forces, poids      |   [[sujet]](pdf/2021/2022_02_28_devoir_forces_mouvement_poids.pdf){target=_blank} [[correction]](pdf/2021/2022_02_28_devoir_forces_mouvement_poids_correction.pdf){target=_blank}|
| 7 et 8      | Atomes, ions, tableau périodique       |   [[sujet]](pdf/2021/2022_04_01_atome_ion_tableau_periodique.pdf){target=_blank} |
| 6 et 7      | Atomes, poids (Brevet blanc 2)     |  [[sujet]](pdf/2021/2022_05_04_brevet_blanc.pdf){target=_blank} [[correction]](pdf/2021/2022_05_04_brevet_blanc_correction.pdf){target=_blank}|
