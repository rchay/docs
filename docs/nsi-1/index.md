# NSI Première

## Ressources

### Logiciels

- [python.org](https://www.python.org/){target=_blank} : site officiel de Python pour installer la dernière version avec IDLE

### Sites

#### Python

- [Python Tutor](https://pythontutor.com/){target=_blank}
- [Basthon](https://basthon.fr/){target=_blank}
- [Pyxel](https://www.pyxelstudio.net/){target=_blank}

#### HTML + CSS + JS

- [JsFiddle](https://jsfiddle.net/){target=_blank}
- [Codepen](https://codepen.io/){target=_blank}

### Pratique

- [France IOI](https://www.france-ioi.org/){target=_blank}
- [Concours Castor](https://castor-informatique.fr/){target=_blank}
- [Concours Algoréa](https://algorea.org/){target=_blank}
- [Codingbat](https://codingbat.com/python){target=_blank}
- [Leetcode](https://leetcode.com/){target=_blank}
- [Future Coder](https://fr.futurecoder.io/){target=_blank}
- [Nuit du code](https://www.nuitducode.net/){target=_blank}

### Youtube

- [Computerphile](https://www.youtube.com/user/Computerphile){target=_blank}
- [Tech with Tim](https://www.youtube.com/channel/UC4JX40jDee_tINbkjycV4Sg){target=_blank}
- [The Coding Train](https://www.youtube.com/channel/UCvjgXvBlbQiydffZU7m1_aw){target=_blank}
- [Crash Course Computer Science](https://www.youtube.com/watch?v=O5nskjZ_GoI&list=PL8dPuuaLjXtNlUrzyH5r6jN9ulIgZBpdo&index=2){target=_blank}

## Devoirs 2023-2024

- Variables, conditions et fonctions [[sujet]](pdf/2023/devoir_2023_09_26.pdf){target=_blank} - [[correction]](pdf/2023/devoir_2023_09_26_correction.pdf){target=_blank}
- Listes [[sujet]](pdf/2023/interro_listes_2.pdf){target=_blank} - [[correction]](pdf/2023/interro_listes_2_correction.pdf){target=_blank}

## Devoirs 2022-2023

- Bases de Python [[sujet]](pdf/2022/interro_bases_de_python.pdf){target=_blank} - [[correction]](pdf/2022/interro_bases_de_python_correction.pdf){target=_blank}
- Algèbre booléenne [[sujet]](pdf/2022/interro_boole.pdf){target=_blank} - [[correction]](pdf/2022/interro_boole_correction.pdf){target=_blank}
- Listes [[sujet]](pdf/2022/interro_listes.pdf){target=_blank} - [[correction]](pdf/2022/interro_listes_correction.pdf){target=_blank}
- Dictionnaires [[sujet]](pdf/2022/interro_dictionnaires.pdf){target=_blank} - [[correction]](pdf/2022/interro_dictionnaires_correction.pdf){target=_blank}
- Entiers naturels [[sujet]](pdf/2022/interro_binaire.pdf){target=_blank}
- Entiers relatifs [[sujet]](pdf/2022/interro_complement_2.pdf){target=_blank}
- Nombres décimaux [[sujet]](pdf/2022/interro_flottants.pdf){target=_blank}
- Bases de Python, binaire, fonctions, conditions [[sujet]](pdf/2022/devoir_2022_11_14.pdf){target=_blank} - [[correction]](pdf/2022/devoir_2022_11_14_correction.pdf){target=_blank}
- Devoir commun 1 [[sujet]](pdf/2022/2023_01_26_devoir_commun_1.pdf){target=_blank} - [[correction]](pdf/2022/2023_01_26_devoir_commun_1_correction.pdf){target=_blank}
- Devoir commun 2 [[sujet]](pdf/2022/devoir_2023_04_27.pdf){target=_blank} - [[correction]](pdf/2022/devoir_2023_04_27_correction.pdf){target=_blank}
