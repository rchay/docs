
# Terminale NSI

- <https://wiki.python.org/moin/TimeComplexity>

## POO

- Trains et voitures : [[sujet]](poo/poo_train.pdf){target=_blank} [[notebook à compléter]](poo/train_eleves.ipynb){download="train_eleves.ipynb"}
- Aéroports, avions et vols : [[sujet]](poo/poo_plane.pdf){target=_blank} [[notebook à compléter]](poo/plane_eleve.ipynb){download="plane_eleves.ipynb"}
