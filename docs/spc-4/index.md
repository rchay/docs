# SPC Quatrième

## Chapitres

- 01 - Atomes et molécules
- 02 - Transformations chimiques
- 03 - L'intensité du courant électrique
- 04 - Les gaz et leurs propriétés
- 05 - La tension électrique
- 06 - La résistance électrique
- 07 - Mouvements et vitesses
- 08 - Le son et les ondes

## Devoirs 2023-2024

| Chapitres | Thèmes | Fichiers |
|:---------:|-------|--------|
|  1 | Atomes et molécules  |   [[sujet a]](2023/2023_09_29_atomes_molecules.pdf){target=_blank} [[sujet b]](2023/2023_10_11_atomes_molecules.pdf){target=_blank}|

## Devoirs 2018-2019

| Chapitres | Thèmes | Fichiers |
|:---------:|-------|--------|
|  1 et 2 | Atomes, molécules et transformations chimiques  |   [[sujet]](2019/2018_10_17_devoir_reaction_atomes.pdf){target=_blank} [[correction]](2019/2018_10_17_devoir_reaction_atomes_correction.pdf){target=_blank} |
| 5ème | Révisions d'électricité   |   [[sujet]](2019/2018_11_22_interro_elec_revisions_a.pdf){target=_blank}  |
| 4 et 5 | Tension électrique, pression d'un gaz  |   [[sujet]](2019/2019_01_23_devoir_elec_atmos.pdf){target=_blank}  |
| 4 | Etats de la matière, masse d'un gaz  |   [[sujet]](2019/2019_02_12_etats_physiques_masse_de_l_air.pdf){target=_blank} [[correction]](2019/2019_02_12_etats_physiques_masse_de_l_air_correction.pdf){target=_blank} |
| 3 et 5  | Lois des circuits électriques  |   [[sujet]](2019/2019_03_28_lois_electricite.pdf){target=_blank} [[correction]](2019/2019_03_28_lois_electricite_correction.pdf){target=_blank} |
| 7 et 8 | Mouvement, vitesse, ondes sonores  |   [[sujet]](2019/2019_05_30_vitesse_mouvement.pdf){target=_blank} [[correction]](2019/2019_05_30_vitesse_mouvement_correction.pdf){target=_blank} |
