# Snake

## Introduction

### Objectifs

L'objectif de ce petit projet initial est de vous familiariser avec la création de jeux vidéos à l'aide du moteur de jeux _rétro_ Pyxel, logiciel libre et open-source.

Le fonctionnement de Pyxel repose sur des constantes et des fonctions prédéfinies qui réalisent automatiquement des actions pour le programmeur.
Pyxel est une bibliothèque instalable dans un terminal avec la commande ou utilisable en ligne sur pyxel studio.

```bash
pip install pyxel
```

0n l’utilise ensuite en important le module au début du script Python :

```python
import pyxel
```

### Cahier des charges

Voici le fonctionnement attendu :

- le serpent se déplace automatiquement, on peut le diriger avec les flèches du clavier.
- s'il mange la pomme, il grandit et la pomme réapparait dans une case vide
- s'il quitte l'écran ou se mord la queue, il meurt, et le jeu s’arrête.

### Principe généraux des jeux vidéos

!!! note Boucle de jeu
    Un jeu vidéo peut être résumé ainsi :
    Une boucle infinie fait progresser le jeu. A chaque tour :
    1. On écoute les interactions du joueur
    1. On met à jour l'état du jeu
    1. On dessine les éléments à l'écran,
    1. On attend quelques millisecondes

    Dans Pyxel, la boucle infinie est implicite, et l’attente des quelques millisecondes déjà prise en charge => pas besoin de s’en occuper.
    Des fonctions prédéfinies gèrent les actions 2 et 3

    |action   |  fonction pyxel  |
    |---|---|
    |mettre à jour l’état du jeu   |  `update` |
    |dessiner les éléments à l’écran   |  `draw` |

Au début du programme, on crée la fenêtre du jeu avec la commande :

```python
pyxel.init(400, 400, title="snake")
```

A la fin du programme, on lance l’exécution du jeu avec :

```python
pyxel.run(update, draw)
```

qui fait appel aux deux fonctions prédéfinies, qui seront appelées 20 fois par seconde.
Il existe de nombreuses méthodes toutes faites permettant de dessiner, écrire du texte… Les couleurs sont désignées par des entiers de 0 à 15 (0 désignant le noir.)
Effacer l’écran et le remplir de noir
pyxel.cls(0)
Détection d’interactions utilisateurs
Flèches clavier
pyxel.btn(pyxel.KEY_RIGHT) ou UP, LEFT, DOWN
barre espace : pyxel.btn(pyxel.KEY_SPACE)
Ecrire du texte
pyxel.text(50,64, 'GAME OVER', 7)
Dessiner un rectangle
pyxel.rect(x, y, long, larg, 1)
x et y : coords du sommet haut gauche. Ensuite les dimensions. Dernier paramètre : la couleur

!!! note Variables globales
    Avec Pyxel, on utilise généralement des variables globales qui sont définies à la racine du script et sont mises à jour dans `update`. (Ce n'est pas une bonne pratique... mais c'est facile) Pour préciser qu'une fonction a le droit de modifier une variable globale, par exemple le score, on écrira `global score` au début de la fonction.
