# Gérer le temps

## Frame count

![](gif/timer_01.gif)

Pyxel compte le nombre d'images (_frames_) qui sont écoulées depuis le lancement du programme. L'instruction `pyxel.frame_count` permet d'accéder à ce numéro de frames écoulées.

Voici un programme simple qui utilise l'instruction `pyxel.text` pour afficher le nombre de frames écoulées au centre de la fenêtre .

Vous pouvez appuyer sur la touche Q pour quitter le programme à n'importe quel moment.

```python
import pyxel

pyxel.init(128, 128)

def update():
    if pyxel.btnp(pyxel.KEY_Q):
        pyxel.quit()

def draw():
    pyxel.cls(0)
    t = pyxel.frame_count
    pyxel.text(64, 64, str(t), 8)
    
pyxel.run(update, draw)
```

!!! note Conversion de type
    L'instruction `pyxel.frame_count` renvoie une valeur entière dont le type python est `int`. Il faut convertir cette valeur en texte avant de pouvoir l'afficher (type python `str`).

## Chronomètre

![](gif/timer_02.gif)

Pyxel affiche 30 frames chaque seconde. En divisant le nombre de frames écoulées par 30, on peut estimer le temps en secondes qui s'est écoulé depuis le lancement du jeu.

```python
import pyxel

pyxel.init(128, 128)


def update():
    if pyxel.btnp(pyxel.KEY_Q):
        pyxel.quit()


def draw():
    pyxel.cls(0)
    t = pyxel.frame_count / 30
    pyxel.text(64, 64, str(t), 8)


pyxel.run(update, draw)
```

![](gif/timer_03.gif)

On arrondi avec l'instruction `round(t, 1)` pour laisser seulement un chiffre après la virgule.

```python
import pyxel


pyxel.init(128, 128)


def update():
    if pyxel.btnp(pyxel.KEY_Q):
        pyxel.quit()


def draw():
    pyxel.cls(0)
    t = round(pyxel.frame_count / 30, 1)
    pyxel.text(64, 64, str(t), 8)


pyxel.run(update, draw)
```

## Remise à zéro

Le compte des frames passées obtenu avec `pyxel.frame_count` est strictement croissant et ne peut pas être remis à zéro (si le joueur veut recommencer une partie par exemple).

Nous allons utiliser une variable `start` pour garder en mémoire le numéro de la frame où le chronomètre a été déclenché et on utilisera ensuite la différence `(pyxel.frame_count - start)` pour calculer le temps écoulé.

Le chronomètre est déclenché lorsque le joueur appuie sur la touche Espace.

``` python
import pyxel


pyxel.init(128, 128)

start = 0
def update():
    global start
    if pyxel.btnp(pyxel.KEY_Q):
        pyxel.quit()
    if pyxel.btnp(pyxel.KEY_SPACE):
        start = pyxel.frame_count

def draw():
    pyxel.cls(0)
    t = round((pyxel.frame_count - start) / 30, 1)
    pyxel.text(64, 64, str(t), 8)
    

pyxel.run(update, draw)

```

## Mise en pause

On souhaite maintenant mettre le chornomètre en pause lorsqu'on appuie sur la touche P.

On utilise une variable booléenne `pause` pour déterminer l'état du chronomètre. Sa valeur est initialisée à `False` car le chronomètre n'est pas en pause lors du lancement du programme. On utilise ensuite l'instruction `pause = not(pause)` pour changer `True` en `False` et `False` en `True` à chaque fois que le joueur appuie sur la touche P.

|`pause` | `not(pause)`|
|--------|-------------|
|`True` | `False`|
|`False` | `True`|

Lorsque le chronomètre est en pause, on incrémente `start` de 1 à chaque frame pour que la différence `(pyxel.frame_count - start)` reste constante.

```python
import pyxel


pyxel.init(128, 128)

start = 0
pause = False


def update():
    global start, pause
    if pyxel.btnp(pyxel.KEY_Q):
        pyxel.quit()
    if pyxel.btnp(pyxel.KEY_SPACE):
        start = pyxel.frame_count
    if pyxel.btnp(pyxel.KEY_P):
        pause = not (pause)
    if pause == True:
        start = start + 1


def draw():
    pyxel.cls(0)
    t = round((pyxel.frame_count - start) / 30, 1)
    pyxel.text(64, 64, str(t), 8)


pyxel.run(update, draw)
```

## Fréquence

![](gif/timer_04.gif)

On peut utiliser `pyxel.frame_count` pour effectuer une action à intervalle de temps régulier. Effectuer une action à chaque seconde est équivalent à éffectuer une action chaque fois que 30 frames sont écoulées.

Pour cela on utilise l'opérateur modulo % pour déterminer si frame_count est un multiple de 30.

On modifie maintenant notre programme pour changer la couleur du texte chaque seconde. Le code couleur est tiré au sort entre 1 et 15 toutes les 30 frames. On exclut le zéro pour ne pas écrire en noir sur un fond noir.

```pyxel
import pyxel
import random

pyxel.init(128, 128)

start = 0
pause = False
colour = 8


def update():
    global start, pause, colour
    if pyxel.btnp(pyxel.KEY_Q):
        pyxel.quit()
    if pyxel.btnp(pyxel.KEY_SPACE):
        start = pyxel.frame_count
    if pyxel.btnp(pyxel.KEY_P):
        pause = not (pause)
    if pause == True:
        start = start + 1
    if pyxel.frame_count % 30 == 0:
        colour = random.randint(1, 15)


def draw():
    pyxel.cls(0)
    t = round((pyxel.frame_count - start) / 30, 1)
    pyxel.text(64, 64, str(t), colour)


pyxel.run(update, draw)
```
