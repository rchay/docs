# Simuler la gravité

## Départ

On commence par un programme simple où les touches Gauche et Droite font bouger un rectangle selon l'horizontale :

```python
import pyxel

pyxel.init(120, 120)

x = 60
y = 60

def update():
    global x , y
    if pyxel.btnp(pyxel.KEY_LEFT):
        x = x - 1
    if pyxel.btnp(pyxel.KEY_RIGHT):
        x = x + 1

def draw():
    pyxel.cls(0)
    pyxel.rect(x, y, 8, 8, 11)

pyxel.run(update, draw)
```

On utilise la touche Espace pour faire sauter le rectangle.

Pour représenter la vitesse du rectangle selon l'axe vertical, on crée une variable vy que l'on initialise à 0. Lorque l'utilisateur presse la touche Espace, on change la valeur de vy à -7. La valeur de vy doit être négative car y diminue lorsque le rectangle se rapproche de la partie supérieure de la fenêtre.

```python
import pyxel

pyxel.init(120, 120)

x = 60
y = 60
v = 0

def update():
    global x, y, v
    if pyxel.btnp(pyxel.KEY_LEFT):
        x = x - 1
    if pyxel.btnp(pyxel.KEY_RIGHT):
        x = x + 1
    if pyxel.btnp(pyxel.KEY_SPACE):
        v = -10
    y = y + v
    

def draw():
    pyxel.cls(0)
    pyxel.rect(x, y, 8, 8, 11)

pyxel.run(update, draw)

```

Le rectangle se déplace vers le haut mais ne redesdend pas.

## Jump

On crée une variable booléeene jump qui va nous permettre de savoir si le personnage est en train de sauter. On initialise sa valeur à False

```python

import pyxel

pyxel.init(120, 120)

x = 60
y = 60
v = 0
jump = False

def update():
    global x, y, v, jump
    if pyxel.btnp(pyxel.KEY_LEFT):
        x = x - 1
    if pyxel.btnp(pyxel.KEY_RIGHT):
        x = x + 1
    if pyxel.btnp(pyxel.KEY_SPACE) and jump == False:
        v = -10
        jump = True

    if jump:
        v +=1
        
    y = y + v
    
def draw():
    pyxel.cls(0)
    pyxel.rect(x, y, 8, 8, 11)

pyxel.run(update, draw)
```
