# SPC Seconde

## Chapitres

| # | Chapitres |
|:---------:|-------|
| 1| Corps purs et mélanges|
| 2| Solutions aqueuses|
| 3| De l'atome à l'élément chimique|
| 4| Emission et perception d'un son|
| 5| Lois de l'électricité|
| 6| Transformations nucléaires|
| 7| Vers des entités chimiques plus stables|
| 8| Quantité de matière|
| 9| Description d'un mouvement|
|10| Modéliser une action mécanique|
|11| Principe d'inertie|
|12| Transformations physiques|
|13| Transformations chimiques|
|14| Réfraction et réflexion de la lumière|
|15| Spectres d'émission|
|16| Lentilles minces|

## Travaux pratiques

| # | Thèmes | Fichiers |
|:---------:|-------|--------|
|1.1  | Préparer une solution hydro-alcoolique | [[sujet]](pdf/tps/seconde_TP_1.1_solution_hydro_alcoolique.pdf)|
|1.2  | Identification d'espèces chimiques | [[sujet]](pdf/tps/seconde_TP_1.2_identification_especes_chimiques.pdf)|
|2.1  | Préparation de solutions par dissolution et par dilution|
|2.2  | Réaliser une échelle de teintes | [[sujet]](pdf/tps/seconde_TP_2.2_echelle_de_teintes.pdf)|
|2.3  | Utilisation de Python en chimie | [[sujet]](pdf/tps/seconde_TP_2.3_python_en_chimie.pdf)|
|3.1  | Identification des ions | [[sujet]](pdf/tps/seconde_TP_3.1_identification_des_ions.pdf)|
|4.1  | Période et fréquence d’un son | [[sujet]](pdf/tps/seconde_TP_4.1_periode_et_frequence.pdf)|
|5.1  | Lois du courant | [[sujet]](pdf/tps/seconde_TP_5.1_lois_du_courant.pdf)|
|5.2  | Caractéristique d'un dipole | [[sujet]](pdf/tps/seconde_TP_5.2_caracteristique_dipole.pdf)|
|6.1  | Transformations nucléaires | [[sujet]](pdf/tps/seconde_TP_6.1_transformations_nucleaires.pdf)|
|7.1  | Les molécules | [[sujet]](pdf/tps/seconde_TP_7.1_molecules.pdf)|
|8.1  | Notion de quantité de matière | [[sujet]](pdf/tps/seconde_TP_8.1_la_mole.pdf)|
|8.2  | La bouteille bleue | [[sujet]](pdf/tps/seconde_TP_8.2_la_bouteille_bleue.pdf)|
|9.1  | La relativité du mouvement | [[sujet]](pdf/tps/seconde_TP_9.1_relativite_du_mouvement_latis.pdf)|
|9.2  | La retrogradation de Mars | [[sujet]](pdf/tps/seconde_TP_9.2_retrogradation_de_mars.pdf)|
|11.1 | Le principe d'inertie|
|13.1 | Synthèse de l'arome de banane | [[sujet]](pdf/tps/seconde_TP_13.1_synthese_arome_banane.pdf)|
|13.2 | Réactif limitant | [[sujet]](pdf/tps/seconde_TP_13.2_reactif_limitant.pdf)|
|13.3 | Coefficients stoechiométriques | [[sujet]](pdf/tps/seconde_TP_13.3_coefficients_stoechiometriques.pdf)|
|13.4 | Horloge à iode | [[sujet]](pdf/tps/seconde_TP_13.4_horloge_iode.pdf)|
|14.1 | Réfraction de la lumière | [[sujet]](pdf/tps/seconde_TP_14.1_refraction.pdf)|
|14.2 | | |
|15.1 | Spectres||
|16.1 | Lentilles minces||

## Fiches d'exercices

| Chapitre | Titre | Fichiers |
|:---------:|-------|--------|
| 1 | Corps purs et mélanges   |    [[sujet]](exo/exercices_seconde_01_corps_pur_et_melanges.pdf){target=_blank} |
| 2 | Préparation de solutions   |    [[sujet]](exo/exercices_seconde_02_solutions.pdf){target=_blank} |
| 3 | Structure de l'atome   |    [[sujet]](exo/exercices_seconde_03_atomes.pdf){target=_blank} |
| 4 | Emission et réception d'un son   |    [[sujet]](exo/exercices_seconde_04_sons.pdf){target=_blank} |
| 5 |    |     |
| 6 | Transformations nucléaires   |    [[sujet]](exo/exercices_seconde_06_reaction_nucleaire.pdf){target=_blank} |
| 7 |    |     |
| 8 | Mole et quantité de matière  |    [[sujet]](exo/exercices_seconde_08_quantite_de_matiere.pdf){target=_blank} |
| 9 |    |     |
| 10 | La gravitation universelle   |    [[sujet]](exo/exercices_seconde_10_gravitation.pdf){target=_blank} |
| 11 | Le principe d'inertie   |    [[sujet]](exo/exercices_seconde_11_inertie.pdf){target=_blank} |
| 12 |    |    |
| 13 | Transformations chimiques   |    [[sujet]](exo/exercices_seconde_13_transformations_chimiques.pdf){target=_blank} |
| 14 | Réfraction de la lumière   |  [[sujet]](exo/exercices_seconde_14_refraction.pdf){target=_blank} |  |
| 15 | Spectres d'émission |    [[sujet]](exo/exercices_seconde_15_spectres.pdf){target=_blank} |
| 16 |    |     |

## Ressources

1. Corps purs et mélanges
    - Composition massique et volumique d'un mélange [[vidéo]](https://www.youtube.com/watch?v=O4zzc7LSSqE){target=_blank}

1. Solutions aqueuses
    - Concentration massique [[vidéo]](https://www.youtube.com/watch?v=13GjBFN79z0){target=_blank}
    - Facteur de dilution [[vidéo]](https://www.youtube.com/watch?v=NUReO9Rkrms){target=_blank}
    - Concentration et saturation [[animation]](https://phet.colorado.edu/sims/html/concentration/latest/concentration_all.html?locale=fr){target=_blank}

1. De l'atome à l'élément chimique
    - Construire un atome [[animation]](https://phet.colorado.edu/sims/html/build-an-atom/latest/build-an-atom_fr.html){target=_blank}

1. Emission et perception d'un son

1. Lois de l'électricité
    - Construire un circuit [[animation]](https://phet.colorado.edu/fr/simulations/circuit-construction-kit-dc-virtual-lab){target=_blank}

1. Transformations nucléaires

1. Vers des entités chimiques plus stables
    - Créer une molécule [[animation]](https://phet.colorado.edu/sims/html/build-a-molecule/latest/build-a-molecule_all.html?locale=fr){target=_blank}

1. Quantité de matière

1. Description d'un mouvement

1. Modéliser une action mécanique

1. Principe d'inertie

1. Transformation physique

1. Transformation chimique
    - Équilibrer des équations chimiques [[animation]](https://phet.colorado.edu/sims/html/balancing-chemical-equations/latest/balancing-chemical-equations_all.html?locale=fr){target=_blank}
    - Réactifs, produits et restes [[animation]](https://phet.colorado.edu/sims/html/reactants-products-and-leftovers/latest/reactants-products-and-leftovers_all.html?locale=fr){target=_blank}

1. Réfraction et réflexion de la lumière
    - Réfraction [[animation]]()
    - Une pièce de monnaie [[vidéo]](https://www.youtube.com/watch?v=Q84FBD6W-ts){target=_blank}
    - La fontaine lumineuse [[vidéo]](https://www.youtube.com/watch?v=iwsLBXW46Uc){target=_blank}

1. Spectres d'émission
      - Spectre du corps noir [[animation]](https://phet.colorado.edu/fr/simulations/blackbody-spectrum){target=_blank}
      - <http://www.ostralo.net/3_animations/swf/spectres.swf>
      - <http://www.ostralo.net/3_animations/swf/spectres_abs_em.swf>

1. Lentilles minces
    - Optique géométrique [[animation]](https://phydemo.app/ray-optics/){target=_blank}

## Devoirs

### Devoirs 2022-2023

| Chapitres | Thèmes | Fichiers |
|:---------:|-------|--------|
| 1 et 2 | Mélanges, solutions aqueuses, concentration massique |    [[sujet]](pdf/devoirs/2022/devoir_2022_10_06.pdf){target=_blank} - [[correction]](pdf/devoirs/2022/devoir_2022_10_06_correction.pdf){target=_blank} |
| 3 et 4 | Modèle de l'atome, ondes sonores, vitesse, période et fréquence | [[sujet]](pdf/devoirs/2022/devoir_2022_11_11.pdf){target=_blank} - [[correction]](pdf/devoirs/2022/devoir_2022_11_11_correction.pdf){target=_blank} |
| 5 et 6 | Lois des circuits, radioactivité | [[sujet]](pdf/devoirs/2022/devoir_2022_12_15.pdf){target=_blank} |
| 7 et 8       | Molécules, isomérie, quantité de matière  | [[sujet]](pdf/devoirs/2022/devoir_2023_02_02.pdf){target=_blank} - [[correction]](pdf/devoirs/2022/devoir_2023_02_02_correction.pdf){target=_blank} |
| 8       | Quantité de matière, masse molaire | [[sujet]](pdf/devoirs/2022/devoir_2023_01_26.pdf){target=_blank} |
| 9, 10 et 11 | Mouvements, forces, principe d'inertie   |  [[sujet]](pdf/devoirs/2022/devoir_2023_03_16.pdf){target=_blank} - [[correction]](pdf/devoirs/2022/devoir_2023_03_16_correction.pdf){target=_blank} |
| 12 et 13 | Transformations physiques, transformations chimiques   | [[sujet]](pdf/devoirs/2022/devoir_2023_05_04.pdf){target=_blank} - [[correction]](pdf/devoirs/2022/devoir_2023_05_04_correction.pdf){target=_blank}  |
| 14 et 15 | Réfraction de la lumière, spectres d'émission   | [[sujet]](pdf/devoirs/2022/devoir_2023_06_01.pdf){target=_blank} - [[correction]](pdf/devoirs/2022/devoir_2023_06_01_correction.pdf){target=_blank}  |

### Devoirs 2021-2022

| Chapitres | Thèmes | Fichiers |
|:---------:|-------|--------|
| 1 et 2 | Mélanges, solutions aqueuses, concentration massique   | [[sujet]](pdf/devoirs/2021/devoir_2021_10_20.pdf){target=_blank} - [[correction]](pdf/devoirs/2021/devoir_2021_10_20_correction.pdf){target=_blank}  |
| 3      | Modèle de l'atome   | [[sujet]](pdf/devoirs/2021/devoir_2021_11_24.pdf){target=_blank} - [[correction]](pdf/devoirs/2021/devoir_2021_11_24_correction.pdf){target=_blank}  |
| 4      | Ondes sonores, vitesse, fréquence et période   | [[sujet]](pdf/devoirs/2021/devoir_2022_01_19.pdf){target=_blank} - [[correction]](pdf/devoirs/2021/devoir_2022_01_19_correction.pdf){target=_blank}  |
| 5 et 7 | Lois des circuits, molécules, isomérie   | [[sujet]](pdf/devoirs/2021/devoir_2022_02_17.pdf){target=_blank}  |
|  8    | Quantité de matière, masse molaire   | [[sujet]](pdf/devoirs/2021/devoir_2022_03_10.pdf){target=_blank} - [[correction]](pdf/devoirs/2021/devoir_2022_03_10_correction.pdf){target=_blank}  |
| 13     | Transformations chimiques   | [[sujet]](pdf/devoirs/2021/devoir_2022_04_27.pdf){target=_blank} - [[correction]](pdf/devoirs/2021/devoir_2022_04_27_correction.pdf){target=_blank}  |
| 9, 10 et 11 | Mouvements, forces, principe d'inertie   | [[sujet]](pdf/devoirs/2021/devoir_2022_06_01.pdf){target=_blank}  |
